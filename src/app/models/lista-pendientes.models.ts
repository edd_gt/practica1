export class ListaPendientes {
  nombre:string;
  pendientes:string[];

  constructor(nombre:string){
    this.nombre = nombre;
    this.pendientes = [];
  }
}
