import { Component, OnInit } from '@angular/core';
import { ListaPendientes } from "./../models/lista-pendientes.models";

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent implements OnInit {

  listaPendientes:ListaPendientes[];

  constructor() {
    this.listaPendientes = [];
   }

  ngOnInit(): void {
  }

  AgregarLista(nombreLista:string){
    this.listaPendientes.push(new ListaPendientes(nombreLista));
  }

}
