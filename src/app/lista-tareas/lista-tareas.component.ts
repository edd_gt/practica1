import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { ListaPendientes } from "./../models/lista-pendientes.models";

@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.css']
})
export class ListaTareasComponent implements OnInit {

  @Input() listaNueva:ListaPendientes;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() { }

  ngOnInit(): void {
  }

  AgregarTarea(tarea:string){
    this.listaNueva.pendientes.push(tarea);
  }

}
